<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Bài 1</title>

    <style>
        .from{
            width: 500px;
            background-color: gray;
            margin: 30px auto;
            padding: 20px;
        }
        .btn-primary{
            margin: auto;
            display: block;
            align-items: center;
        }
        .from h1{
            display: block;
            margin: 10 auto;
            text-align: center;
        }
    </style>
</head>
<body>

<?php
    if (isset($_POST['Diem1'])) {
        $Diem1 = $_POST['Diem1'];
        if (isset($_POST['Diem2'])) {
            $Diem2 = $_POST['Diem2'];
        }
    }else{
        $Diem1 = 0;
        $Diem2 = 0;
    }

    function avg($a, $b){
        $Avg = ($a + 2*$b)/3;
        // echo$Avg;
        return$Avg;
    }

    $DTB = avg($Diem1, $Diem2);
    function Kq($DTB){
        if ($DTB > 5) {
            echo 'Được lên lớp';
        }
        else{
            echo 'Ở lại lớp';
        }
        return;
    }

    function XepLoai($DTB){
        if ($DTB >= 8) {
            echo 'Học sinh Giỏi';
        }
        else{
            if ($DTB > 6.5 and $DTB < 8) {
                echo 'Học sinh Khá';
            }
            else {
                if ($DTB >= 5 and $DTB <= 6.5) {
                    echo 'Học sinh Trung bình';
                }
                else {
                    if ($DTB <5 ) {
                        echo 'Học sinh Yếu';
                    }
                }
            }
        }
        return;
    }
?>

<div class="from">
<h2>Kết quả học tập</h2>
    <form action="" method="POST">
        <div class="form-group">
            <label for="">Điểm học kỳ I</label>
            <input type="text" class="form-control" placeholder="Điểm học kỳ I" required value ="<?=$Diem1 ?>" name="Diem1">
        </div>
        <div class="form-group">
            <label for="">Điểm học kỳ II</label>
            <input type="text" class="form-control" placeholder="Điểm học kỳ II" required value ="<?=$Diem2 ?>" name="Diem2">
        </div>
        <div class="form-group">
            <label for="">Điểm trung bình</label>
            <input type="text" class="form-control" placeholder="Điểm trung bình" value=" <?php echo avg($Diem1, $Diem2) ?>" disabled>
        </div>
        <div class="form-group">
            <label for="" >Kết quả</label>
            <input type="text" class="form-control" placeholder="Kết quả"  value =" <?php Kq($DTB) ?>" disabled>
        </div>
        <div class="form-group">
            <label for="">Xếp loại học lực </label>
            <input class="form-control" type="munber" placeholder="Xếp loại học lực" value =" <?php XepLoai($DTB) ?> " disabled ></input>
        </div>
        <button type="submit" class="btn btn-primary">Xem kết quả</button>
    </form>
</div>
</body>
</html>